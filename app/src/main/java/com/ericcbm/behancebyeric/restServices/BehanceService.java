package com.ericcbm.behancebyeric.restServices;

import com.ericcbm.behancebyeric.models.Collection;
import com.ericcbm.behancebyeric.models.Projects;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Eric Mesquita on 24/02/2016.
 */
public interface BehanceService {
    String URL_ENDPOINT = "https://www.behance.net/v2/";

    // 17225945  //10083305  //47536521
    @GET("/collections/10083305/projects?api_key=dYM384VlJ36Cnd9cG3i7orsfUytaRmgx")
    void getCollection(@Query("page") int page, Callback<Collection> cb);

    @GET("/projects/{projectId}?api_key=dYM384VlJ36Cnd9cG3i7orsfUytaRmgx")
    void getProject(@Path("projectId") int projectId, Callback<Projects> cb);
}
