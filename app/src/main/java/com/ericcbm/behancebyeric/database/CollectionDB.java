package com.ericcbm.behancebyeric.database;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

/**
 * Created by Eric Mesquita on 24/02/2016.
 */
@Table(name = "collection")
public class CollectionDB extends Model {
    @Column(name = "page")
    public int page;
    @Column(name = "json")
    public String json;

    public static CollectionDB getByPage(int page) {
        return new Select()
                .from(CollectionDB.class)
                .where("page = ?", page)
                .executeSingle();
    }
}
