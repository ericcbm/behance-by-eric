package com.ericcbm.behancebyeric.models;

import java.util.List;

/**
 * Created by Eric Mesquita on 24/02/2016.
 */
public class Collection {
    private int http_code;
    private List<Projects> projects;

    public int getHttp_code() {
        return http_code;
    }

    public void setHttp_code(int http_code) {
        this.http_code = http_code;
    }

    public List<Projects> getProjects() {
        return projects;
    }

    public void setProjects(List<Projects> projects) {
        this.projects = projects;
    }

}
