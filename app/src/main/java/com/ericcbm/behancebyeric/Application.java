package com.ericcbm.behancebyeric;

import com.activeandroid.ActiveAndroid;

/**
 * Created by Eric Mesquita on 25/02/2016.
 */
public class Application extends android.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
    }
}
