package com.ericcbm.behancebyeric.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ericcbm.behancebyeric.R;
import com.ericcbm.behancebyeric.models.Projects;
import com.ericcbm.behancebyeric.transform.CropSquareTransformation;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Eric Mesquita on 24/02/2016.
 */
public class ProjectsAdapter extends RecyclerView.Adapter<ProjectsAdapter.ViewHolder> {

    private ArrayList<Projects> projects;
    Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView textViewProjectTitle;
        public ImageView imageViewProject;
        public TextView textViewViewsCount;

        public ViewHolder(View v) {
            super(v);
            textViewProjectTitle = (TextView) v.findViewById(R.id.textViewProjectTitle);
            imageViewProject = (ImageView) v.findViewById(R.id.imageViewProject);
            textViewViewsCount = (TextView) v.findViewById(R.id.textViewViewsCount);
        }
    }

    public ProjectsAdapter(Context context, ArrayList<Projects> myDataset) {
        this.context = context;
        projects = myDataset;
    }

    @Override
    public ProjectsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_project, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        Projects project = projects.get(position);
        holder.textViewProjectTitle.setText(project.getName());
        Picasso.with(holder.imageViewProject.getContext()).load(project.getCovers().getOriginal()).placeholder(R.drawable.picture_placeholder).transform(new CropSquareTransformation()).into(holder.imageViewProject);
        holder.textViewViewsCount.setText(String.valueOf(project.getStats().getViews()));
    }

    @Override
    public int getItemCount() {
        return projects.size();
    }
}
