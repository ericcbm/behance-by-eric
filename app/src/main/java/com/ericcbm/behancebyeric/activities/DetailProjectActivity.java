package com.ericcbm.behancebyeric.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Transition;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ericcbm.behancebyeric.R;
import com.ericcbm.behancebyeric.models.Projects;
import com.ericcbm.behancebyeric.models.Projects.Owner;
import com.ericcbm.behancebyeric.transform.CircleTransform;
import com.ericcbm.behancebyeric.transform.CropSquareTransformation;
import com.ericcbm.behancebyeric.views.ExpandableHeightGridView;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_detail_project)
@OptionsMenu(R.menu.detail_project)
public class DetailProjectActivity extends AppCompatActivity {

    // Extra name for the ID parameter
    public static final String EXTRA_PARAM_JSON = "detail:_json";

    // View name of the header image. Used for activity scene transitions
    public static final String VIEW_NAME_HEADER_IMAGE = "detail:header:image";

    @ViewById
    Toolbar toolbar;
    @ViewById
    ImageView backdrop;
    @ViewById(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @ViewById(R.id.textview_count)
    TextView textViewViewsCount;
    @ViewById(R.id.textview_likes)
    TextView textViewViewsLikes;
    @ViewById(R.id.relativeLayout_info)
    RelativeLayout relativeLayoutInfo;
    @ViewById
    FloatingActionButton fab;
    @ViewById(R.id.grid)
    ExpandableHeightGridView gridViewOwners;
    GridAdapter gridAdapter;

    private Projects project;


    @AfterViews
    void calledAfterViewInjection() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String projectJson = getIntent().getStringExtra(EXTRA_PARAM_JSON);
        Gson gson = new Gson();
        project = gson.fromJson(projectJson, Projects.class);

        collapsingToolbar.setTitle(project.getName());

        gridViewOwners.setExpanded(true);
        gridAdapter = new GridAdapter();
        gridViewOwners.setAdapter(gridAdapter);
        gridAdapter.notifyDataSetChanged();

        gridViewOwners.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Owner owner = (Owner) parent.getItemAtPosition(position);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(owner.getUrl())));
            }
        });

        /**
         * Set the name of the view's which will be transition to, using the static values above.
         * This could be done in the layout XML, but exposing it via static variables allows easy
         * querying from other Activities
         */
        ViewCompat.setTransitionName(backdrop, VIEW_NAME_HEADER_IMAGE);

        textViewViewsCount.setText(Integer.toString(project.getStats().getViews()));
        textViewViewsLikes.setText(Integer.toString(project.getStats().getAppreciations()));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && addTransitionListener()) {
            // If we're running on Lollipop and we have added a listener to the shared element
            // transition, load the thumbnail. The listener will load the full-size image when
            // the transition is complete.
            loadThumbnail();
        } else {
            // If all other cases we should just load the full-size image now
            loadFullSizeImage();
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(project.getUrl())));
            }
        });
    }

    @OptionsItem(R.id.action_share)
    void openWebPage() {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, project.getName() + " - " + project.getUrl());
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.action_share)));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Private Methods

    /**
     * Load the item's thumbnail image into our {@link ImageView}.
     */
    private void loadThumbnail() {
        Picasso.with(backdrop.getContext())
                .load(project.getCovers().getOriginal())
                .noFade()
                .transform(new CropSquareTransformation())
                .into(backdrop);
    }

    /**
     * Load the item's full-size image into our {@link ImageView}.
     */
    private void loadFullSizeImage() {
        Picasso.with(backdrop.getContext())
                .load(project.getCovers().getOriginal())
                .noFade()
                .noPlaceholder()
                .transform(new CropSquareTransformation())
                .into(backdrop);
    }

    /**
     * Try and add a {@link Transition.TransitionListener} to the entering shared element
     * {@link Transition}. We do this so that we can load the full-size image after the transition
     * has completed.
     *
     * @return true if we were successful in adding a listener to the enter transition
     */
    private boolean addTransitionListener() {
        final Transition transition = getWindow().getSharedElementEnterTransition();

        if (transition != null) {
            // There is an entering shared element transition so add a listener to it
            transition.addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionEnd(Transition transition) {
                    // As the transition has ended, we can now load the full-size image
                    loadFullSizeImage();

                    // Make sure we remove ourselves as a listener
                    transition.removeListener(this);
                }

                @Override
                public void onTransitionStart(Transition transition) {
                    // No-op
                }

                @Override
                public void onTransitionCancel(Transition transition) {
                    // Make sure we remove ourselves as a listener
                    transition.removeListener(this);
                }

                @Override
                public void onTransitionPause(Transition transition) {
                    // No-op
                }

                @Override
                public void onTransitionResume(Transition transition) {
                    // No-op
                }
            });
            return true;
        }

        // If we reach here then we have not added a listener
        return false;
    }

    /**
     * {@link android.widget.BaseAdapter} which displays items.
     */
    private class GridAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return project.getOwners().size();
        }

        @Override
        public Owner getItem(int position) {
            return project.getOwners().get(position);
        }

        @Override
        public long getItemId(int position) {
            return  project.getOwners().indexOf(getItem(position));
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = getLayoutInflater().inflate(R.layout.grid_item_owners, viewGroup, false);
            }

            final Owner owner = getItem(position);

            // Load the thumbnail image
            ImageView image = (ImageView) view.findViewById(R.id.imageview_item);
            Picasso.with(image.getContext()).load(owner.getImages().getImage()).placeholder(R.mipmap.profile_placeholder).transform(new CircleTransform()).into(image);

            TextView name = (TextView) view.findViewById(R.id.textview_name);
            name.setText(owner.getUsername());

            return view;
        }
    }

}
