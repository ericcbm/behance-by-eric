package com.ericcbm.behancebyeric.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ericcbm.behancebyeric.R;
import com.ericcbm.behancebyeric.models.Projects;
import com.ericcbm.behancebyeric.restServices.BehanceService;
import com.ericcbm.behancebyeric.adapters.ProjectsAdapter;
import com.ericcbm.behancebyeric.database.CollectionDB;
import com.ericcbm.behancebyeric.models.Collection;
import com.ericcbm.behancebyeric.utils.ItemClickSupport;
import com.google.gson.Gson;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

@EActivity(R.layout.activity_collections)
public class CollectionsActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SwipeRefreshLayout.OnRefreshListener {

    @ViewById
    Toolbar toolbar;
    @ViewById(R.id.drawer_layout)
    DrawerLayout drawer;
    @ViewById(R.id.nav_view)
    NavigationView navigationView;
    @ViewById
    ProgressBar progressBar;
    @ViewById
    RecyclerView recyclerView;
    @ViewById
    TextView textViewMessage;
    @ViewById(R.id.swipe_container)
    SwipeRefreshLayout mSwipeLayout;

    private ArrayList<Projects> projects;
    private RecyclerView.Adapter adapter;

    // Infinite Scroll state
    private int visibleItemCount;
    private int totalItemCount;
    private int pastVisiblesItems;
    private int currentPage = 0;
    private boolean loading = true;

    @AfterViews
    void calledAfterViewInjection() {
        init();
        loadCollectionByPage(1);
    }

    @Background
    void loadCollectionByPage(final int page) {
        showTopProgress(true);
        showMessage(false, "");

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BehanceService.URL_ENDPOINT)
                .build();

        BehanceService service = restAdapter.create(BehanceService.class);

        service.getCollection(page, new Callback<Collection>() {
            @Override
            public void success(Collection collection, Response response) {
                before();
                if (collection.getHttp_code() == 200) {
                    showProjects(collection.getProjects(), currentPage);
                    Gson gson = new Gson();
                    CollectionDB collectionDB = new CollectionDB();
                    collectionDB.page = page;
                    collectionDB.json = gson.toJson(collection);
                    collectionDB.save();
                    currentPage++;
                } else if (currentPage == 0) {
                    showMessage(true, "Não foi possível carregar a collection.");
                }
                after();
            }

            @Override
            public void failure(RetrofitError error) {
                before();
                CollectionDB collectionBD = CollectionDB.getByPage(page);
                if (collectionBD != null) {
                    Gson gson = new Gson();
                    Collection collection = gson.fromJson(collectionBD.json, Collection.class);
                    showProjects(collection.getProjects(), currentPage);
                    currentPage++;
                } else if (currentPage == 0) {
                    showMessage(true, "Erro ao carregar a collection. Verifique sua conexão.");
                }
                after();
            }

            public void before() {
                showProgress(false);
                showTopProgress(false);
                showMessage(false, "");
            }

            public void after() {
                mSwipeLayout.setRefreshing(false);
                loading = true;
            }
        });
    }

    @UiThread
    void showProjects(List<Projects> projects, int pageNumber) {
        if (pageNumber == 0) {
            this.projects.clear();
        }
        for (Projects project : projects) {
            this.projects.add(project);
        }
        adapter.notifyDataSetChanged();
    }

    @UiThread
    void showTopProgress(boolean show) {
        mSwipeLayout.setRefreshing(show);
    }

    @UiThread
    void showProgress(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }

    @UiThread
    void showMessage(boolean show, String message) {
        textViewMessage.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
        textViewMessage.setText(message);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onRefresh() {
        currentPage = 0;
        loadCollectionByPage(1);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_linkedin) {
            try {
                getPackageManager().getPackageInfo("com.linkedin.android", 0);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("linkedin://ericcbm")));
            } catch (Exception e) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.linkedin.com/in/ericcbm")));
            }
        } else if (id == R.id.nav_facebook) {
            try {
                getPackageManager().getPackageInfo("com.facebook.katana", 0);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/1275605727")));
            } catch (Exception e) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/ericcbm")));
            }
        } else if (id == R.id.nav_twitter) {
            try {
                getPackageManager().getPackageInfo("com.twitter.android", 0);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=ericcbm")));
            } catch (Exception e) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/ericcbm")));
            }
        } else if (id == R.id.nav_instagram) {
            try {
                getPackageManager().getPackageInfo("com.instagram.android", 0);
                Uri uri = Uri.parse("http://instagram.com/_u/ericcbm");
                Intent intentInsta = new Intent(Intent.ACTION_VIEW, uri);
                intentInsta.setPackage("com.instagram.android");
                startActivity(intentInsta);
            } catch (Exception e) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://instagram.com/ericcbm")));
            }
        } else if (id == R.id.nav_email) {
            openEmailToSend();
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //Private Methods

    private void init() {
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        mSwipeLayout.setOnRefreshListener(this);
        mSwipeLayout.setColorSchemeResources(
                android.R.color.holo_blue_dark, android.R.color.holo_purple, android.R.color.holo_green_dark, android.R.color.holo_orange_dark);
        mSwipeLayout.setRefreshing(true);
        initRecyclerView();
    }

    private void initRecyclerView() {
        projects = new ArrayList<>();
        adapter = new ProjectsAdapter(this, projects);
        recyclerView.setAdapter(adapter);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = mLayoutManager.getChildCount();
                    totalItemCount = mLayoutManager.getItemCount();
                    pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            loadCollectionByPage(currentPage + 1);
                        }
                    }
                }
            }
        });
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                Projects project = projects.get(position);
                Gson gson = new Gson();
                String projectJson = gson.toJson(project, Projects.class);

                Intent intent = new Intent(CollectionsActivity.this, DetailProjectActivity_.class);
                intent.putExtra(DetailProjectActivity.EXTRA_PARAM_JSON, projectJson);
                ActivityOptionsCompat activityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(
                        CollectionsActivity.this,

                        // Now we provide a list of Pair items which contain the view we can transitioning
                        // from, and the name of the view it is transitioning to, in the launched activity
                        new Pair<View, String>(v.findViewById(R.id.imageViewProject),
                                DetailProjectActivity.VIEW_NAME_HEADER_IMAGE));

                // Now we can start the Activity, providing the activity options as a bundle
                ActivityCompat.startActivity(CollectionsActivity.this, intent, activityOptions.toBundle());
            }
        });
    }

    private void openEmailToSend() {
        String[] mailTo = new String[]{"ericcbm@gmail.com"};
        String stringUri = "mailto:" + TextUtils.join(",", mailTo) + "?subject=Contato";
        Uri builtUri = Uri.parse(stringUri);
        //Log.v(PlayerService.LOG_TAG, "Built URI " + builtUri.toString());
        Intent intent;
        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2) {
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(builtUri);
        } else {
            intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:"));
            intent.putExtra(Intent.EXTRA_SUBJECT, "Contato");
            intent.putExtra(Intent.EXTRA_EMAIL, mailTo);
        }
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Toast.makeText(this, "Houve um erro, verifique se possui algum app de email.", Toast.LENGTH_LONG).show();
        }
    }
}
